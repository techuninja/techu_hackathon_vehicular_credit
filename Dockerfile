FROM openjdk:11-jre-slim-buster
EXPOSE 8082
ARG JAR_FILE=target/vehicular_credit-1.0.0.jar
ADD ${JAR_FILE} vehicular_credit.jar
ENTRYPOINT ["java","-jar","/vehicular_credit.jar"]