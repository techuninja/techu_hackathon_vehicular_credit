package com.techu.hackathon.vehicular_credit.controller;

import com.techu.hackathon.vehicular_credit.dto.PayServiceDto;
import com.techu.hackathon.vehicular_credit.service.VehicleCreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/credit/vehicular")
public class VehicleCreditController {

    @Autowired
    VehicleCreditService service;

    @PostMapping(value = "/{service}/pay", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> payService(@PathVariable("service") String serviceId, @RequestBody PayServiceDto serviceDto) {
        service.payService(serviceId, serviceDto);
        return new ResponseEntity<>(null, HttpStatus.CREATED);
    }
}
