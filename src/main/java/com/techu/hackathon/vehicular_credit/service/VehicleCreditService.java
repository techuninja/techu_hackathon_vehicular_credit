package com.techu.hackathon.vehicular_credit.service;

import com.techu.hackathon.vehicular_credit.dto.PayServiceDto;

public interface VehicleCreditService {

    public void payService(String serviceId, PayServiceDto serviceDto);
}
