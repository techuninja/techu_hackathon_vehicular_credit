package com.techu.hackathon.vehicular_credit.service;

import com.techu.hackathon.vehicular_credit.dto.BalanceDto;
import com.techu.hackathon.vehicular_credit.dto.PayServiceDto;
import com.techu.hackathon.vehicular_credit.enums.Uris;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class VehicleCreditServiceImpl implements VehicleCreditService {

    @Autowired
    RestTemplate template;

    @Override
    public void payService(String serviceId, PayServiceDto serviceDto) {
        String url = new StringBuilder(Uris.ACCOUNT_SERVICE.getUri())
                .append(String.format("/%s", serviceDto.getAccountId()))
                .append("/charge")
                .toString();
        BalanceDto balanceDto = new BalanceDto(serviceDto.getAmount(), serviceDto.getCurrency(), serviceDto.getCustomerId(), serviceDto.getProductId());
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        ResponseEntity<Object> response = template.exchange(url, HttpMethod.PATCH, new HttpEntity<>(balanceDto, headers), Object.class);
        //System.out.println(response.getBody());
    }
}
