package com.techu.hackathon.vehicular_credit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@Component
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException {

        HttpStatus status = httpResponse.getStatusCode();
        return status.is4xxClientError() || status.is5xxServerError();
    }

    @Override
    public void handleError(ClientHttpResponse httpResponse) throws IOException {

        if (httpResponse.getStatusCode().is5xxServerError()) {
            throw new ServerException("Error de sistema");
        } else if (httpResponse.getStatusCode().is4xxClientError()) {
            throw new ClientException("Error de cliente");
        } else {
            throw new NotFoundException("Error desconocido");
        }
    }

    static class ServerException extends IOException {
        public ServerException(String message) {
            super(message);
        }
    }

    static class ClientException extends IOException {
        public ClientException(String message) {
            super(message);
        }
    }

    static class NotFoundException extends IOException {
        public NotFoundException(String message) {
            super(message);
        }
    }
}
