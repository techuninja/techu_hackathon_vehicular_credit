package com.techu.hackathon.vehicular_credit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ControllerException extends ResponseEntityExceptionHandler {

    /*@ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleError(Exception e){
        return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }*/

    @ExceptionHandler(value = {RestTemplateResponseErrorHandler.NotFoundException.class})
    public ResponseEntity<Map<String, String>> handleNotFoundError(RestTemplateResponseErrorHandler.NotFoundException e) {
        return new ResponseEntity<>(buildError(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(RestTemplateResponseErrorHandler.ServerException.class)
    public ResponseEntity<Map<String, String>> handleServerError(RestTemplateResponseErrorHandler.ServerException e) {
        return new ResponseEntity<>(buildError(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(RestTemplateResponseErrorHandler.ClientException.class)
    public ResponseEntity<Map<String, String>> handleClientError(RestTemplateResponseErrorHandler.ClientException e) {
        return new ResponseEntity<>(buildError(e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    private Map<String, String> buildError(String message){
        Map<String, String> mapError = new HashMap<>();
        mapError.put("error", message);
        return mapError;
    }
}
