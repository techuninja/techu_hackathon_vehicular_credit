package com.techu.hackathon.vehicular_credit;

import com.techu.hackathon.vehicular_credit.exception.RestTemplateResponseErrorHandler;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class VehicularCreditApplication {

    public static void main(String[] args) {
        SpringApplication.run(VehicularCreditApplication.class, args);
    }

    @Bean
    public RestTemplate createTemplate(){
        RestTemplate template = new RestTemplate();
        HttpClient httpClient = HttpClientBuilder.create().build();
        template.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));
        template.setErrorHandler(new RestTemplateResponseErrorHandler());
        return template;
    }
}
