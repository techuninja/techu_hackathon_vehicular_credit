package com.techu.hackathon.vehicular_credit.enums;

public enum Uris {

    //ACCOUNT_SERVICE("http://localhost:8081/accounts");
    ACCOUNT_SERVICE("http://ec2-54-188-92-53.us-west-2.compute.amazonaws.com:8081/accounts");

    private String uri;

    Uris(String uri) {
        this.uri = uri;
    }

    public String getUri(){
        return uri;
    }
}
