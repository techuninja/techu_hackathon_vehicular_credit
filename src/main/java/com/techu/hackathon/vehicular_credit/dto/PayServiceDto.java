package com.techu.hackathon.vehicular_credit.dto;

public class PayServiceDto {

    String accountId;

    double amount;

    String currency;

    String customerId;

    String productId;

    public PayServiceDto(String accountId, double amount, String currency, String customerId, String productId) {
        this.accountId = accountId;
        this.amount = amount;
        this.currency = currency;
        this.customerId = customerId;
        this.productId = productId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
